﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using WPF_TestTool.General;
using WPF_TestTool.GUI;
using WPF_TestTool.Model;

namespace WPF_TestTool.UC
{
    /// <summary>
    /// Interaction logic for UC_Editor.xaml
    /// </summary>
    public partial class UC_Editor : UC_Base
    {
        public delegate void StartRun(int startIndex, List<Step> lstSteps);
        public StartRun startRun;
        public delegate void StopRun();
        public StopRun stopRun;

        int id = 0;
        string name = Define.Instance.Control;
        string fileName = string.Empty;
        BindingList<ControlData> lstControls = new BindingList<ControlData>();
        frmMain frmMain;

        public UC_Editor()
        {
            InitializeComponent();
        }
        public UC_Editor(frmMain _frmMain)
        {
            InitializeComponent();
            frmMain = _frmMain;
        }

        protected override void UC_Base_Loaded(object sender, RoutedEventArgs e)
        {
            frmMain.sendStatus = new frmMain.SendStatus(new Action<int, string>((index, msg) =>
            {
                switch (index)
                {
                    case -2:
                        //Start
                        //btnUp.Enabled = false;
                        //btnDown.Enabled = false;
                        //cbbCommand.Enabled = false;
                        //btnAdd.Enabled = false;
                        //lbControls.Enabled = false;
                        //btnNew.Enabled = false;
                        //btnLoad.Enabled = false;
                        //btnSave.Enabled = false;
                        //btnRun.Enabled = false;
                        //btnStop.Enabled = true;
                        break;
                    case -1:
                        //End
                        //btnUp.Enabled = true;
                        //btnDown.Enabled = true;
                        //cbbCommand.Enabled = true;
                        //btnAdd.Enabled = true;
                        //lbControls.Enabled = true;
                        //btnNew.Enabled = true;
                        //btnLoad.Enabled = true;
                        //btnSave.Enabled = true;
                        //btnRun.Enabled = true;
                        //btnStop.Enabled = false;
                        break;
                    default:
                        //Items
                        lbControls.SelectedIndex = index;
                        break;
                }
            }));

            //cbbCommand.Items.AddRange(new string[] { Define.Instance.Url_Text, Define.Instance.Mouse_Text, Define.Instance.Input_Text, Define.Instance.Keys_Text, Define.Instance.Display_Text, Define.Instance.UploadFile_Text, Define.Instance.Alert_Text, Define.Instance.Properties_Text });
            //lbControls.DataSource = lstControls;
            //lbControls.DisplayMember = "Text";
            //lbControls.ValueMember = "Name";

            LoadData(string.Empty);
            InitEvent();
            FormatForm();
            FormatControl();
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            StartProcess();
        }
        private void BtnLoad_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(fileName) && frmMain.showConfirm(Caption: "File is opened. Save it?"))
                SaveData(fileName);
            LoadFile();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            SaveFile();
        }
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            //switch (cbbCommand.SelectedIndex)
            //{
            //    case 0:
            //        //Url
            //        AddUrlControl();
            //        break;
            //    case 1:
            //        //Mouse
            //        AddMouseControl();
            //        break;
            //    case 2:
            //        //Input
            //        AddInputControl();
            //        break;
            //    case 3:
            //        //Key
            //        AddKeyControl();
            //        break;
            //    case 4:
            //        //Display
            //        AddDisplayControl();
            //        break;
            //    case 5:
            //        //Upload file
            //        AddUploadFileControl();
            //        break;
            //    case 6:
            //        //Alert
            //        AddAlertControl();
            //        break;
            //    case 7:
            //        //Properties
            //        AddPropertiesControl();
            //        break;
            //}
        }
        private void LbControls_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
            {
                //cMenu_Delete.Enabled = false;
                //if (lbControls.SelectedIndex != -1)
                //    cMenu_Delete.Enabled = true;
            }
        }
        private void CMenu_Delete_Click(object sender, EventArgs e)
        {
            DeleteControl();
        }
        private void LbControls_DoubleClick(object sender, EventArgs e)
        {
            ShowControl();
        }
        private void BtnDown_Click(object sender, EventArgs e)
        {
            if (lbControls.SelectedIndex < lbControls.Items.Count - 1)
            {
                ControlData item = lstControls[lbControls.SelectedIndex];
                lstControls[lbControls.SelectedIndex] = lstControls[lbControls.SelectedIndex + 1];
                lstControls[lbControls.SelectedIndex + 1] = item;

                lbControls.SelectedIndex++;
            }
        }
        private void BtnUp_Click(object sender, EventArgs e)
        {
            if (lbControls.SelectedIndex > 0)
            {
                ControlData item = lstControls[lbControls.SelectedIndex];
                lstControls[lbControls.SelectedIndex] = lstControls[lbControls.SelectedIndex - 1];
                lstControls[lbControls.SelectedIndex - 1] = item;

                lbControls.SelectedIndex--;
            }
        }
        private void BtnNew_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(fileName) && frmMain.showConfirm(Caption: "File is opened. Save it?"))
                SaveData(fileName);

            LoadData(string.Empty);
        }
        private void BtnStop_Click(object sender, EventArgs e)
        {
            StopProcess();
        }
        private void TbarDelay_ValueChanged(object sender, EventArgs e)
        {
            //Define.Instance.DelayValue = tbarDelay.Value;
        }

        void InitEvent()
        {
            //Disable
            btnRun.Click -= btnRun_Click;
            btnLoad.Click -= BtnLoad_Click;
            btnSave.Click -= BtnSave_Click;
            btnAdd.Click -= BtnAdd_Click;
            lbControls.MouseDown -= LbControls_MouseDown;
            //lbControls.DoubleClick -= LbControls_DoubleClick;
            //cMenu_Delete.Click -= CMenu_Delete_Click;
            btnUp.Click -= BtnUp_Click;
            btnDown.Click -= BtnDown_Click;
            btnNew.Click -= BtnNew_Click;
            btnStop.Click -= BtnStop_Click;
            tbarDelay.ValueChanged -= TbarDelay_ValueChanged;

            //Enable
            btnRun.Click += btnRun_Click;
            btnLoad.Click += BtnLoad_Click;
            btnSave.Click += BtnSave_Click;
            btnAdd.Click += BtnAdd_Click;
            lbControls.MouseDown += LbControls_MouseDown;
            //lbControls.DoubleClick += LbControls_DoubleClick;
            //cMenu_Delete.Click += CMenu_Delete_Click;
            btnUp.Click += BtnUp_Click;
            btnDown.Click += BtnDown_Click;
            btnNew.Click += BtnNew_Click;
            btnStop.Click += BtnStop_Click;
            tbarDelay.ValueChanged += TbarDelay_ValueChanged;
        }
        void LoadFile()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = ".xml|*.xml";
            bool? result = dialog.ShowDialog(frmMain);
            if (result.HasValue && result == true)
            {
                LoadData(dialog.FileName);
                //btnRun.Select();
            }
        }
        void SaveFile()
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = ".xml|*.xml";
                dialog.FileName = $"{DateTime.Now.ToString("yyyyMMdd-hhmmss-ttt")}";
                bool? result = dialog.ShowDialog(frmMain);
                if (result.HasValue && result == true)
                {
                    //Text = dialog.FileName;
                    SaveData(dialog.FileName);
                    //btnRun.Select();
                }
            }
            else
            {
                SaveData(fileName);
                //btnRun.Select();
            }
        }
        void LoadData(string fileName)
        {
            id = 0;
            this.fileName = fileName;
            lstControls.Clear();
            pnControl.Children.Clear();

            //if (!File.Exists(fileName))
            //    return;

            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(fileName);
            //XmlNode xmlPage = xmlDoc.SelectSingleNode($"/{Define.Instance.Page}");

            //if (xmlPage == null)
            //    return;

            ///*Load XML*/
            //XmlNodeList xmlControls = xmlPage.SelectNodes($"{Define.Instance.Control}");

            //foreach (XmlNode xmlControl in xmlControls)
            //{
            //    string type = string.Empty;

            //    if (xmlControl.Attributes[Define.Instance.Type] != null)
            //        type = xmlControl.Attributes[Define.Instance.Type].Value;

            //    if (type.ToEqualEx(Define.Instance.Input))
            //    {
            //        AddInputControl(new UC_InputHandle(xmlControl));
            //    }
            //    else if (type.ToEqualEx(Define.Instance.Mouse))
            //    {
            //        AddMouseControl(new UC_MouseHandle(xmlControl));
            //    }
            //    else if (type.ToEqualEx(Define.Instance.Keys))
            //    {
            //        AddKeyControl(new UC_KeyHandle(xmlControl));
            //    }
            //    else if (type.ToEqualEx(Define.Instance.Display))
            //    {
            //        AddDisplayControl(new UC_DisplayHandle(xmlControl));
            //    }
            //    else if (type.ToEqualEx(Define.Instance.Url))
            //    {
            //        AddUrlControl(new UC_UrlHandle(xmlControl));
            //    }
            //    else if (type.ToEqualEx(Define.Instance.UploadFile))
            //    {
            //        AddUploadFileControl(new UC_UploadFileHandle(xmlControl));
            //    }
            //    else if (type.ToEqualEx(Define.Instance.Alert))
            //    {
            //        AddAlertControl(new UC_AlertHandle(xmlControl));
            //    }
            //    else if (type.ToEqualEx(Define.Instance.Properties))
            //    {
            //        AddPropertiesControl(new UC_PropertiesHandle(xmlControl));
            //    }
            //}
        }
        void SaveData(string fileName)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();

                this.fileName = fileName;
                if (System.IO.File.Exists(fileName))
                    xmlDoc.Load(fileName);
                else
                    xmlDoc.InitDocument();

                XmlNode xmlPage = xmlDoc.SelectSingleNode($"/{Define.Instance.Page}");
                xmlPage.RemoveAll();

                /*Save XML*/
                //foreach (ControlData cd in lstControls)
                //{
                //    Control ctrControl = cd.Control;

                //    if (ctrControl is UC_MouseHandle)
                //    {
                //        UC_MouseHandle uc = ctrControl as UC_MouseHandle;
                //        uc.SaveData(xmlDoc, xmlPage);
                //    }
                //    else if (ctrControl is UC_InputHandle)
                //    {
                //        UC_InputHandle uc = ctrControl as UC_InputHandle;
                //        uc.SaveData(xmlDoc, xmlPage);
                //    }
                //    else if (ctrControl is UC_KeyHandle)
                //    {
                //        UC_KeyHandle uc = ctrControl as UC_KeyHandle;
                //        uc.SaveData(xmlDoc, xmlPage);
                //    }
                //    else if (ctrControl is UC_DisplayHandle)
                //    {
                //        UC_DisplayHandle uc = ctrControl as UC_DisplayHandle;
                //        uc.SaveData(xmlDoc, xmlPage);
                //    }
                //    else if (ctrControl is UC_UrlHandle)
                //    {
                //        UC_UrlHandle uc = ctrControl as UC_UrlHandle;
                //        uc.SaveData(xmlDoc, xmlPage);
                //    }
                //    else if (ctrControl is UC_UploadFileHandle)
                //    {
                //        UC_UploadFileHandle uc = ctrControl as UC_UploadFileHandle;
                //        uc.SaveData(xmlDoc, xmlPage);
                //    }
                //    else if (ctrControl is UC_AlertHandle)
                //    {
                //        UC_AlertHandle uc = ctrControl as UC_AlertHandle;
                //        uc.SaveData(xmlDoc, xmlPage);
                //    }
                //    else if (ctrControl is UC_PropertiesHandle)
                //    {
                //        UC_PropertiesHandle uc = ctrControl as UC_PropertiesHandle;
                //        uc.SaveData(xmlDoc, xmlPage);
                //    }
                //}

                xmlDoc.Save(fileName);
            }
            catch (Exception ex)
            {
                ex.Log();
                frmMain.showError(Caption: ex.Message);
            }
        }
        void StartProcess()
        {
            if (lbControls.Items.Count == 0)
                return;

            List<Step> lstSteps = new List<Step>();

            //for (int i = lbControls.SelectedIndex; i < lbControls.Items.Count; i++)
            //{
            //    Control ctrControl = lstControls[i].Control;

            //    Step Step = new Step();
            //    Step.Name = ctrControl.Text;
            //    Step.Status = false;

            //    if (ctrControl is UC_MouseHandle)
            //    {
            //        UC_MouseHandle uc = ctrControl as UC_MouseHandle;
            //        Step.Func = uc.GetFunc();
            //        lstSteps.Add(Step);
            //    }
            //    else if (ctrControl is UC_InputHandle)
            //    {
            //        UC_InputHandle uc = ctrControl as UC_InputHandle;
            //        Step.Func = uc.GetFunc();
            //        lstSteps.Add(Step);
            //    }
            //    else if (ctrControl is UC_KeyHandle)
            //    {
            //        UC_KeyHandle uc = ctrControl as UC_KeyHandle;
            //        Step.Func = uc.GetFunc();
            //        lstSteps.Add(Step);
            //    }
            //    else if (ctrControl is UC_DisplayHandle)
            //    {
            //        UC_DisplayHandle uc = ctrControl as UC_DisplayHandle;
            //        Step.Func = uc.GetFunc();
            //        lstSteps.Add(Step);
            //    }
            //    else if (ctrControl is UC_UrlHandle)
            //    {
            //        UC_UrlHandle uc = ctrControl as UC_UrlHandle;
            //        Step.Func = uc.GetFunc();
            //        lstSteps.Add(Step);
            //    }
            //    else if (ctrControl is UC_UploadFileHandle)
            //    {
            //        UC_UploadFileHandle uc = ctrControl as UC_UploadFileHandle;
            //        Step.Func = uc.GetFunc();
            //        lstSteps.Add(Step);
            //    }
            //    else if (ctrControl is UC_AlertHandle)
            //    {
            //        UC_AlertHandle uc = ctrControl as UC_AlertHandle;
            //        Step.Func = uc.GetFunc();
            //        lstSteps.Add(Step);
            //    }
            //    else if (ctrControl is UC_PropertiesHandle)
            //    {
            //        UC_PropertiesHandle uc = ctrControl as UC_PropertiesHandle;
            //        Step.Func = uc.GetFunc();
            //        lstSteps.Add(Step);
            //    }
            //}
            startRun?.Invoke(lbControls.SelectedIndex, lstSteps);
        }
        void StopProcess()
        {
            stopRun?.Invoke();
        }
        //void AddMouseControl(UC_MouseHandle uc = null)
        //{
        //    pnControl.Controls.Clear();
        //    id++;

        //    if (uc == null)
        //    {
        //        uc = new UC_MouseHandle();
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Mouse_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);
        //        uc.Show();

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Mouse, Control = uc });
        //        pnControl.Controls.Add(uc);
        //        lbControls.SelectedValue = uc.Name;
        //    }
        //    else
        //    {
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Mouse_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Mouse, Control = uc });
        //    }
        //}
        //void AddInputControl(UC_InputHandle uc = null)
        //{
        //    pnControl.Controls.Clear();
        //    id++;

        //    if (uc == null)
        //    {
        //        uc = new UC_InputHandle();
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Input_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);
        //        uc.Show();

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Input, Control = uc });
        //        pnControl.Controls.Add(uc);
        //        lbControls.SelectedValue = uc.Name;
        //    }
        //    else
        //    {
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Input_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Input, Control = uc });
        //    }

        //}
        //void AddKeyControl(UC_KeyHandle uc = null)
        //{
        //    pnControl.Controls.Clear();
        //    id++;

        //    if (uc == null)
        //    {
        //        uc = new UC_KeyHandle();
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Keys_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);
        //        uc.Show();

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Keys, Control = uc });
        //        pnControl.Controls.Add(uc);
        //        lbControls.SelectedValue = uc.Name;
        //    }
        //    else
        //    {
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Keys_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Keys, Control = uc });
        //    }
        //}
        //void AddDisplayControl(UC_DisplayHandle uc = null)
        //{
        //    pnControl.Controls.Clear();
        //    id++;

        //    if (uc == null)
        //    {
        //        uc = new UC_DisplayHandle();
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Display_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);
        //        uc.Show();

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Display, Control = uc });
        //        pnControl.Controls.Add(uc);
        //        lbControls.SelectedValue = uc.Name;
        //    }
        //    else
        //    {
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Display_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Display, Control = uc });
        //    }
        //}
        //void AddUrlControl(UC_UrlHandle uc = null)
        //{
        //    pnControl.Controls.Clear();
        //    id++;

        //    if (uc == null)
        //    {
        //        uc = new UC_UrlHandle();
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Url_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);
        //        uc.Show();

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Url, Control = uc });
        //        pnControl.Controls.Add(uc);
        //        lbControls.SelectedValue = uc.Name;
        //    }
        //    else
        //    {
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Url_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Url, Control = uc });
        //    }
        //}
        //void AddUploadFileControl(UC_UploadFileHandle uc = null)
        //{
        //    pnControl.Controls.Clear();
        //    id++;

        //    if (uc == null)
        //    {
        //        uc = new UC_UploadFileHandle();
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.UploadFile_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);
        //        uc.Show();

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.UploadFile, Control = uc });
        //        pnControl.Controls.Add(uc);
        //        lbControls.SelectedValue = uc.Name;
        //    }
        //    else
        //    {
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.UploadFile_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.UploadFile, Control = uc });
        //    }
        //}
        //void AddAlertControl(UC_AlertHandle uc = null)
        //{
        //    pnControl.Controls.Clear();
        //    id++;

        //    if (uc == null)
        //    {
        //        uc = new UC_AlertHandle();
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Alert_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);
        //        uc.Show();

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Alert, Control = uc });
        //        pnControl.Controls.Add(uc);
        //        lbControls.SelectedValue = uc.Name;
        //    }
        //    else
        //    {
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Alert_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Alert, Control = uc });
        //    }
        //}
        //void AddPropertiesControl(UC_PropertiesHandle uc = null)
        //{
        //    pnControl.Controls.Clear();
        //    id++;

        //    if (uc == null)
        //    {
        //        uc = new UC_PropertiesHandle();
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Properties_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);
        //        uc.Show();

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Properties, Control = uc });
        //        pnControl.Controls.Add(uc);
        //        lbControls.SelectedValue = uc.Name;
        //    }
        //    else
        //    {
        //        uc.Name = $"{name}{id}";
        //        uc.Text = $"{Define.Instance.Properties_Text} {id}";
        //        uc.Dock = DockStyle.Fill;
        //        uc.SetTitle(uc.Text);

        //        lstControls.Add(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Properties, Control = uc });
        //    }
        //}
        void EditControl(Control control)
        {
            pnControl.Children.Clear();
            pnControl.Children.Add(control);
            lbControls.SelectedValue = control.Name;
        }
        void ShowControl()
        {
            if (lbControls.SelectedIndex == -1)
                return;

            //ControlData cd = lstControls[lbControls.SelectedIndex];
            //switch (cd.Type)
            //{
            //    case Define.Instance.Mouse:
            //    case Define.Instance.Input:
            //    case Define.Instance.Keys:
            //    case Define.Instance.Url:
            //    case Define.Instance.Display:
            //    case Define.Instance.UploadFile:
            //    case Define.Instance.Alert:
            //    case Define.Instance.Properties:
            //        EditControl(cd.Control);
            //        break;
            //}
        }
        void DeleteControl()
        {
            if (lbControls.SelectedIndex == -1)
                return;

            ControlData cd = lstControls[lbControls.SelectedIndex];

            //if (pnControl.Controls[cd.Name] != null)
            //    pnControl.Controls.RemoveByKey(cd.Name);

            lstControls.Remove(cd);
        }
        public override void FormatControl()
        {
            gsMain.Format(new Thickness(Define.Instance.Margin), 3, double.NaN);
            btnLoad.Format(new Thickness(Define.Instance.Margin));
            btnNew.Format(new Thickness(Define.Instance.Margin));
            btnRun.Format(new Thickness(Define.Instance.Margin));
            btnSave.Format(new Thickness(Define.Instance.Margin));
            btnStop.Format(new Thickness(Define.Instance.Margin));
            tbarDelay.Format(new Thickness(Define.Instance.Margin));
            cbbCommand.Format(new Thickness(0, Define.Instance.Margin, Define.Instance.Margin, Define.Instance.Margin), double.NaN);
            btnAdd.Format(new Thickness(Define.Instance.Margin, Define.Instance.Margin, Define.Instance.Margin, Define.Instance.Margin), 25);
            btnDown.Format(new Thickness(Define.Instance.Margin, Define.Instance.Margin, Define.Instance.Margin, Define.Instance.Margin), 25);
            btnUp.Format(new Thickness(Define.Instance.Margin, Define.Instance.Margin, 0, Define.Instance.Margin), 25);

            tbarDelay.Format(10, 10000, 100);
            gsMain.Format();

            tbarDelay.DataContext = Define.Instance;
        }
    }
}
