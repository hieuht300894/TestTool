﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using WPF_TestTool.General;

namespace WPF_TestTool.UC
{
    public class UC_Base : UserControl
    {
        public UC_Base()
        {
            Loaded -= UC_Base_Loaded;
            SizeChanged -= UC_Base_SizeChanged;

            Loaded += UC_Base_Loaded;
            SizeChanged += UC_Base_SizeChanged;
        }

        protected virtual void UC_Base_SizeChanged(object sender, SizeChangedEventArgs e)
        {
        }
        protected virtual void UC_Base_Loaded(object sender, RoutedEventArgs e)
        {
        }

        public virtual void FormatForm()
        {
            Width = double.NaN;
            Height = double.NaN;
        }
        public virtual void FormatControl()
        {

        }
    }
}
