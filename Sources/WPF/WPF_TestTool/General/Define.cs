﻿using System;

namespace WPF_TestTool.General
{
    public class Define
    {
        static readonly Define instance = new Define();
        public static Define Instance { get { return instance; } }

        //Mouse handle
        public string Click { get; private set; } = "click";
        public string MouseDown { get; private set; } = "mousedown";
        public string MouseUp { get; private set; } = "mouseup";
        public string MouseOver { get; private set; } = "mouseover";
        public string MouseMove { get; private set; } = "mousemove";
        public string MouseOut { get; private set; } = "mouseout";
        public string DblClick { get; private set; } = "dblclick";

        //Tagname
        public string Page { get; private set; } = "page";
        public string Control { get; private set; } = "control";
        public string Title { get; private set; } = "title";

        //Attribute
        public string Type { get; private set; } = "type";
        public string Mouse { get; private set; } = "mouse";
        public string Keys { get; private set; } = "keys";
        public string Display { get; private set; } = "display";
        public string Url { get; private set; } = "url";
        public string UploadFile { get; private set; } = "uploadfile";
        public string Input { get; private set; } = "input";
        public string Alert { get; private set; } = "alert";
        public string Properties { get; private set; } = "properties";
        public string Property { get; private set; } = "property";
        public string Handle { get; private set; } = "handle";
        public string Target { get; private set; } = "target";
        public string Value { get; private set; } = "value";
        public string IsPosition { get; private set; } = "isposition";
        public string PositionX { get; private set; } = "positionx";
        public string PositionY { get; private set; } = "positiony";
        public string Timeout { get; private set; } = "timeout";
        public string Path { get; private set; } = "path";
        public string Text { get; private set; } = "text";
        public string Button { get; private set; } = "button";
        public string OK { get; private set; } = "ok";
        public string Cancel { get; private set; } = "cancel";
        public string ID { get; private set; } = "id";
        public string Key { get; private set; } = "key";
        public string Displayed { get; private set; } = "displayed";
        public string Enabled { get; private set; } = "enabled";
        public string Selected { get; private set; } = "selected";
        public string Location { get; private set; } = "location";
        public string Size { get; private set; } = "size";
        public string Width { get; private set; } = "width";
        public string Height { get; private set; } = "height";

        //Default Value
        public int TimeoutValue { get; private set; } = 3;
        public int DelayValue { get; set; } = 50;
        public string dir { get; private set; } = "log";
        public string errorFile { get; private set; } = $"error-{DateTime.Now.ToString("yyyyMMdd")}.txt";
        public string logFile { get; private set; } = $"log-{DateTime.Now.ToString("yyyyMMdd")}.txt";
        public int Start { get; private set; } = -2;
        public int Stop { get; private set; } = -1;
        public int Abort { get; private set; } = 0;
        public double Margin { get; private set; } = 3;

        //Display Text
        public string Mouse_Text { get; private set; } = "Mouse";
        public string Keys_Text { get; private set; } = "Keys";
        public string Display_Text { get; private set; } = "Display";
        public string Url_Text { get; private set; } = "Url";
        public string UploadFile_Text { get; private set; } = "Upload File";
        public string Input_Text { get; private set; } = "Input";
        public string Alert_Text { get; private set; } = "Alert";
        public string SimpleAlert_Text { get; private set; } = "Simple Alert";
        public string PromptAlert_Text { get; private set; } = "Prompt Alert";
        public string ConfirmationAlert_Text { get; private set; } = "Confirmation Alert";
        public string Properties_Text { get; private set; } = "Properties";

        //Alert
        public string SimpleAlert { get; private set; } = "simple";
        public string PromptAlert { get; private set; } = "prompt";
        public string ConfirmationAlert { get; private set; } = "confirmation";
    }
}
