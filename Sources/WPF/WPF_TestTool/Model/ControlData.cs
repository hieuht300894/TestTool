﻿namespace WPF_TestTool.Model
{
    public class ControlData
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        //public Control Control { get; set; }
    }
}
