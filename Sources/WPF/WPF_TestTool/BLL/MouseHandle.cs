﻿using OpenQA.Selenium;
using System.Text;

namespace WPF_TestTool.BLL
{
    public static class MouseHandle
    {
        public static void InitMouseHandle(this IWebDriver webDriver)
        {
            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine("function MouseHandle(_element, _eventType) {");
            sbScript.AppendLine("    var clickEvent = document.createEvent('MouseEvents');");
            sbScript.AppendLine("    clickEvent.initMouseEvent('_eventType', true, true);");
            sbScript.AppendLine("    _element.dispatchEvent(clickEvent);");
            sbScript.AppendLine("    console.log(_element);");
            sbScript.AppendLine("    console.log(clickEvent.type);");
            sbScript.AppendLine("}");

            IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            var a = js.ExecuteScript(sbScript.ToString());
        }
        public static void SendMouseHandle(this IWebDriver webDriver, IWebElement targetElement, string eventType)
        {
            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"var clickEvent = document.createEvent('MouseEvents');");
            sbScript.AppendLine($"clickEvent.initMouseEvent('{eventType}', true, true);");
            sbScript.AppendLine($"arguments[0].dispatchEvent(clickEvent);");
            sbScript.AppendLine($"console.log(clickEvent.type);");

            IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            var a = js.ExecuteScript(sbScript.ToString(), targetElement);

            //StringBuilder sbScript = new StringBuilder();
            //sbScript.AppendLine("function MouseHandle(_element, _eventType) {");
            //sbScript.AppendLine("    var clickEvent = document.createEvent('MouseEvents');");
            //sbScript.AppendLine("    clickEvent.initMouseEvent(_eventType, true, true);");
            //sbScript.AppendLine("    _element.dispatchEvent(clickEvent);");
            //sbScript.AppendLine("    console.log(_element);");
            //sbScript.AppendLine("    console.log(clickEvent.type);");
            //sbScript.AppendLine("}");
            //sbScript.AppendLine($"MouseHandle(arguments[0], '{eventType}');");

            //IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            //var a = js.ExecuteScript(sbScript.ToString(), targetElement);
        }
        public static void SendMouseHandle(this IWebDriver webDriver, string eventType, int x, int y)
        {
            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"var clickEvent = document.createEvent('MouseEvents');");
            sbScript.AppendLine($"var _element = document.elementFromPoint({x}, {y});");
            sbScript.AppendLine($"clickEvent.initMouseEvent(");
            sbScript.AppendLine($"/*type*/ '{eventType}',");
            sbScript.AppendLine($"/*canBubble*/ true,");
            sbScript.AppendLine($"/*cancelable*/ true,");
            sbScript.AppendLine($"/*view*/ window,");
            sbScript.AppendLine($"/*detail*/ 0,");
            sbScript.AppendLine($"/*screenX*/ 0,");
            sbScript.AppendLine($"/*screenY*/ 0,");
            sbScript.AppendLine($"/*clientX*/ {x},");
            sbScript.AppendLine($"/*clientY*/ {y},");
            sbScript.AppendLine($"/*ctrlKey*/ false,");
            sbScript.AppendLine($"/*altKey*/ false,");
            sbScript.AppendLine($"/*shiftKey*/ false,");
            sbScript.AppendLine($"/*metaKey*/ false,");
            sbScript.AppendLine($"/*button*/ 0,");
            sbScript.AppendLine($"/*relatedTarget*/ null);");
            sbScript.AppendLine($"_element.dispatchEvent(clickEvent);");
            sbScript.AppendLine($"console.log(clickEvent.type);");

            IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            var a = js.ExecuteScript(sbScript.ToString());
        }

        //public static JavascriptResponse SendMouseHandle(this ChromiumWebBrowser browser, string eventType, int x, int y)
        //{
        //    return browser.EvaluateScriptAsync($"MouseHandlePoint(document, '{eventType}', {x}, {y});").Result;
        //}
    }
}
