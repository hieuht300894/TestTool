﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_TestTool.GUI
{
    public partial class frmBase : Form
    {
        public frmBase()
        {
            InitializeComponent();

            Load -= FrmBase_Load;
            Load += FrmBase_Load;
            FormClosing -= FrmBase_FormClosing;
            FormClosing += FrmBase_FormClosing;
            FormClosed -= FrmBase_FormClosed;
            FormClosed += FrmBase_FormClosed;
            SizeChanged -= FrmBase_SizeChanged;
            SizeChanged += FrmBase_SizeChanged;
            Enter -= FrmBase_Enter;
            Enter += FrmBase_Enter;
        }

        protected virtual void FrmBase_Enter(object sender, EventArgs e)
        {
        }

        protected virtual void FrmBase_SizeChanged(object sender, EventArgs e)
        {
        }

        protected virtual void FrmBase_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        protected virtual void FrmBase_FormClosing(object sender, FormClosingEventArgs e)
        {
            GC.Collect();
        }

        protected virtual void FrmBase_Load(object sender, EventArgs e)
        {
        }
    }
}
