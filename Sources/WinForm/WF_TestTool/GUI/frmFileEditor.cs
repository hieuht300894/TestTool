﻿using Common;
using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using WF_TestTool.UC;

namespace WF_TestTool.GUI
{
    public partial class frmFileEditor : frmBase
    {
        public delegate void SendPath(string text);
        public SendPath sendPath;

        string path = string.Empty;
        string extension = string.Empty;
        string text = string.Empty;

        public frmFileEditor()
        {
            InitializeComponent();
        }
        public frmFileEditor(string path)
        {
            InitializeComponent();
            this.path = path;
        }

        protected override void FrmBase_Load(object sender, EventArgs e)
        {
            tpMain.Controls.Clear();
            UC_FileEditor uc = new UC_FileEditor(path);
            uc.Dock = DockStyle.Fill;
            uc.sendText = LoadText;
            tpMain.Controls.Add(uc);

            Text = path;
        }

        void LoadText(string text)
        {
            this.text = text;
            SaveFile();
        }
        void SaveFile()
        {
            if (string.IsNullOrWhiteSpace(path) || !File.Exists(path))
            {
                extension = Path.GetExtension(path);

                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = extension.IsNotEmpty() ? $"{extension}|*{extension}" : Define.Instance.filterAllFiles;
                dialog.FileName = $"{DateTime.Now.ToString("yyyyMMdd-hhmmss-ttt")}";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    Text = dialog.FileName;
                    path = dialog.FileName;
                    SaveData();
                }
            }
            else
            {
                SaveData();
            }
        }
        void SaveData()
        {
            try
            {
                if (Extension.SaveText(path, text))
                    sendPath?.Invoke(path);
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                this.showError(Caption: ex.Message);
            }
        }
    }
}
