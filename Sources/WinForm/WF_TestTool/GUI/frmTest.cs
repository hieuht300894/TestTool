﻿using Common;
using SeleniumAPI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using WF_TestTool.UC;

namespace WF_TestTool.GUI
{
    public partial class frmTest : frmBase
    {
        public frmTest()
        {
            InitializeComponent();
        }

        protected override void FrmBase_Load(object sender, EventArgs e)
        {
            ChromeHandle.StartChromeDriver();

            StringBuilder html = new StringBuilder();

            html.AppendLine("");


            for (int i = 1; i <= 1; i++)
            {
                ChromeHandle.GoToUrl($"https://www.vnjpclub.com/minna-no-nihongo/bai-{i}-tu-vung.html");
                string _html = ChromeHandle.GetHTMLBy("//*[@id=\"rt-mainbody\"]/div/article/div/table/tbody").Replace("\t", string.Empty);

                html.AppendLine($"<bai{i}>");
                html.AppendLine($"<tu_vung>");
                html.AppendLine($"{_html}");
                html.AppendLine($"</tu_vung>");
                html.AppendLine($"</bai{i}>");

                XmlDocument document = new XmlDocument();
                document.LoadXml(_html);


            }
        }
        protected override void FrmBase_FormClosing(object sender, FormClosingEventArgs e)
        {
            base.FrmBase_FormClosing(sender, e);

            ChromeHandle.ShutdownChromeDriver();
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }
    }
}
