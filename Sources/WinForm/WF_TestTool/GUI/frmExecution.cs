﻿using Common;
using SeleniumAPI;
using System;
using System.Windows.Forms;
using WF_TestTool.UC;

namespace WF_TestTool.GUI
{
    public partial class frmExecution : frmBase
    {
        public delegate void ShowFormMain();
        public ShowFormMain showFormMain;

        UC_Execution uc = null;
        Action closeWindow = null;

        public frmExecution()
        {
            InitializeComponent();
        }

        protected override void FrmBase_Load(object sender, EventArgs e)
        {
            uc = new UC_Execution();
            uc.Dock = DockStyle.Fill;

            tpMain.Controls.Clear();
            tpMain.Controls.Add(uc);

            closeWindow = closeWindow ?? new Action(() =>
            {
                FormClosing -= FrmBase_FormClosing;

                ChromeHandle.ShutdownChromeDriver();

                if (showFormMain == null)
                    Application.Exit();
                else
                    showFormMain();
            });
        }
        protected override void FrmBase_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (uc.GetStatus() == Define.eStatus.Start)
                uc.Abort();

            if (uc.GetStatus() == Define.eStatus.Stop)
            {
                uc.ClearData();
                base.FrmBase_FormClosing(sender, e);
                closeWindow.Invoke();
            }
            else
                e.Cancel = true;
        }
    }
}
