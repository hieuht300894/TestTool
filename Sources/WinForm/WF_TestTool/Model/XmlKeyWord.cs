﻿using Common;
using System.Collections.Generic;
using System.Linq;

namespace WF_TestTool.Model
{
    public class XmlKeyWord
    {
        static List<XMLTagNameData> tagNames;
        static List<XMLAttributeNameData> attributeNames;

        public static List<XMLTagNameData> GetTagNames()
        {
            if (tagNames == null)
            {
                tagNames = new List<XMLTagNameData>();
                tagNames.Add(new XMLTagNameData() { Text = $"{Define.Instance.Step}" });
                tagNames.Add(new XMLTagNameData() { Text = $"{Define.Instance.Input}" });
                tagNames.Add(new XMLTagNameData() { Text = $"{Define.Instance.Key}" });
                tagNames.Add(new XMLTagNameData() { Text = $"{Define.Instance.Property}" });
            }
            return tagNames;
        }
        public static List<XMLAttributeNameData> GetAttributeNames()
        {
            if (attributeNames == null)
            {
                attributeNames = new List<XMLAttributeNameData>();
                attributeNames.Add(new XMLAttributeNameData() { Text = $"{Define.Instance.ID}" });
                attributeNames.Add(new XMLAttributeNameData() { Text = $"{Define.Instance.Target}" });
                attributeNames.Add(new XMLAttributeNameData() { Text = $"{Define.Instance.Type}" });
                attributeNames.Add(new XMLAttributeNameData() { Text = $"{Define.Instance.Target}" });
                attributeNames.Add(new XMLAttributeNameData() { Text = $"{Define.Instance.Type}" });
                attributeNames.Add(new XMLAttributeNameData() { Text = $"{Define.Instance.Target}" });
                attributeNames.Add(new XMLAttributeNameData() { Text = $"{Define.Instance.Type}" });
                attributeNames.Add(new XMLAttributeNameData() { Text = $"{Define.Instance.Target}" });
                attributeNames.Add(new XMLAttributeNameData() { Text = $"{Define.Instance.Type}" });
                attributeNames.Add(new XMLAttributeNameData() { Text = $"{Define.Instance.Target}" });
            }
            return attributeNames;
        }
        public static List<XMLTagNameData> SearchTagNames(char letter)
        {
            return GetTagNames().Where(x => x.Text.ToStartsWithEx(letter.ToString())).ToList();
        }
        public static List<XMLAttributeNameData> SearchAttributeNames(char letter)
        {
            return GetAttributeNames().Where(x => x.Text.ToStartsWithEx(letter.ToString())).ToList();
        }
    }
}
