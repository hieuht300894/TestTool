﻿using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace WF_TestTool.Model
{
    public class Colorize : DocumentColorizingTransformer
    {
        /* 
         * lineNumber
         * startOffset
         * endOffset
         */
        Tuple<int, int, int>[] elements;

        public Colorize(params Tuple<int, int, int>[] elements)
        {
            this.elements = elements != null ? elements : new Tuple<int, int, int>[] { };
        }

        protected override void ColorizeLine(DocumentLine line)
        {
            Tuple<int, int, int> element = elements.FirstOrDefault(x => x.Item1 == line.LineNumber);
            if (element != null && !line.IsDeleted && line.LineNumber == element.Item1 && line.Offset <= element.Item2 && line.EndOffset >= element.Item3)
                ChangeLinePart(element.Item2, element.Item3, ApplyChanges);
        }
        void ApplyChanges(VisualLineElement element)
        {
            // apply changes here
            element.TextRunProperties.SetBackgroundBrush(Brushes.LightGreen);
        }
    }
}
