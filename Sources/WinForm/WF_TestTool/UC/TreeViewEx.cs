﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_TestTool.UC
{
    [ToolboxItem(true)]
    public class TreeViewEx : TreeView
    {
        List<TreeNode> lstNodes;

        public TreeViewEx()
        {
            lstNodes = new List<TreeNode>();
        }

        public void Add(TreeNode node)
        {
            Nodes.Add(node);
            lstNodes.Add(node);
        }
        public void Add(TreeNode parent, params TreeNode[] childs)
        {
            parent.Nodes.AddRange(childs ?? new TreeNode[] { });
            lstNodes.AddRange(childs ?? new TreeNode[] { });
        }
        public List<TreeNode> Get()
        {
            return lstNodes;
        }
        public void Clear()
        {
            Nodes.Clear();
            lstNodes.Clear();
        }
    }
}
