﻿using Common;
using Common.Models;
using SeleniumAPI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WF_TestTool.Model;

namespace WF_TestTool.UC
{
    public partial class UC_Editor : UC_Base
    {
        int id = 0;
        int delay = Define.Instance.DelayValue;
        string name = Define.Instance.Step;
        string fileName = string.Empty;
        Define.eStatus iStatus = Define.eStatus.Stop;
        List<CustomMethod> handles = new List<CustomMethod>();
        List<Step> steps = new List<Step>();

        public UC_Editor()
        {
            InitializeComponent();
        }

        protected override void UC_Base_Load(object sender, EventArgs e)
        {
            tbarDelay.Value = Define.Instance.DelayValue;

            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Url,
                Text = Define.Instance.Url_Text,
                ControlType = typeof(UC_UrlHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_UrlHandle), typeof(XmlNode) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_UrlHandle), typeof(int) }, null),
                SaveData = typeof(UC_UrlHandle).GetMethod(nameof(SaveData), BindingFlags.Public | BindingFlags.Instance)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Mouse,
                Text = Define.Instance.Mouse_Text,
                ControlType = typeof(UC_MouseHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_MouseHandle), typeof(XmlNode) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_MouseHandle), typeof(int) }, null),
                SaveData = typeof(UC_MouseHandle).GetMethod(nameof(SaveData), BindingFlags.Public | BindingFlags.Instance)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Input,
                Text = Define.Instance.Input_Text,
                ControlType = typeof(UC_InputHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_InputHandle), typeof(XmlNode) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_InputHandle), typeof(int) }, null),
                SaveData = typeof(UC_InputHandle).GetMethod(nameof(SaveData), BindingFlags.Public | BindingFlags.Instance)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Key,
                Text = Define.Instance.Key_Text,
                ControlType = typeof(UC_KeyHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_KeyHandle), typeof(XmlNode) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_KeyHandle), typeof(int) }, null),
                SaveData = typeof(UC_KeyHandle).GetMethod(nameof(SaveData), BindingFlags.Public | BindingFlags.Instance)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Display,
                Text = Define.Instance.Display_Text,
                ControlType = typeof(UC_DisplayHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_DisplayHandle), typeof(XmlNode) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_DisplayHandle), typeof(int) }, null),
                SaveData = typeof(UC_DisplayHandle).GetMethod(nameof(SaveData), BindingFlags.Public | BindingFlags.Instance)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Upload,
                Text = Define.Instance.Upload_Text,
                ControlType = typeof(UC_UploadFileHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_UploadFileHandle), typeof(XmlNode) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_UploadFileHandle), typeof(int) }, null),
                SaveData = typeof(UC_UploadFileHandle).GetMethod(nameof(SaveData), BindingFlags.Public | BindingFlags.Instance)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Alert,
                Text = Define.Instance.Alert_Text,
                ControlType = typeof(UC_AlertHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_AlertHandle), typeof(XmlNode) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_AlertHandle), typeof(int) }, null),
                SaveData = typeof(UC_AlertHandle).GetMethod(nameof(SaveData), BindingFlags.Public | BindingFlags.Instance)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Properties,
                Text = Define.Instance.Properties_Text,
                ControlType = typeof(UC_PropertiesHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_PropertiesHandle), typeof(XmlNode) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_PropertiesHandle), typeof(int) }, null),
                SaveData = typeof(UC_PropertiesHandle).GetMethod(nameof(SaveData), BindingFlags.Public | BindingFlags.Instance)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Popup,
                Text = Define.Instance.Popup_Text,
                ControlType = typeof(UC_PopupHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_PopupHandle), typeof(XmlNode) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_PopupHandle), typeof(int) }, null),
                SaveData = typeof(UC_PopupHandle).GetMethod(nameof(SaveData), BindingFlags.Public | BindingFlags.Instance)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.ReadFile,
                Text = Define.Instance.ReadFile_Text,
                ControlType = typeof(UC_ReadFilesHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_ReadFilesHandle), typeof(XmlNode) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_ReadFilesHandle), typeof(int) }, null),
                SaveData = typeof(UC_ReadFilesHandle).GetMethod(nameof(SaveData), BindingFlags.Public | BindingFlags.Instance)
            });
            handles.Add(new CustomMethod()
            {
                Name = Define.Instance.Scroll,
                Text = Define.Instance.Scroll_Text,
                ControlType = typeof(UC_ScrollHandle),
                AddControl = GetType().GetMethod(nameof(AddControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_ScrollHandle), typeof(XmlNode) }, null),
                GetControl = GetType().GetMethod(nameof(GetControl), BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(UC_ScrollHandle), typeof(int) }, null),
                SaveData = typeof(UC_ScrollHandle).GetMethod(nameof(SaveData), BindingFlags.Public | BindingFlags.Instance)
            });
            cbbCommand.DataSource = handles;
            cbbCommand.FormatEx("Name", "Text");

            lvControls.Columns.Add("colName", "Name", 200);
            lvControls.ContextMenuStrip = cMenu;
            lvControls.Format();

            LoadData(string.Empty);
            InitEvent();
        }
        protected override void UC_Base_Enter(object sender, EventArgs e)
        {
            tbarDelay.Value = delay;
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            Start();
        }
        private void BtnLoad_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(fileName) && this.showConfirm(Caption: "File is opened. Save it?"))
                SaveData(fileName);
            LoadFile();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            SaveFile();
        }
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            CustomMethod handle = cbbCommand.SelectedItem as CustomMethod;
            if (handle == null)
                return;

            handle.ExecuteMethod(handle.AddControl, this, new object[] { null, null });
        }
        private void LvControls_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                cMenu_Delete.Enabled = false;
                if (lvControls.GetItemAt(e.Location.X, e.Location.Y) != null)
                    cMenu_Delete.Enabled = true;
            }
        }
        private void CMenu_Delete_Click(object sender, EventArgs e)
        {
            DeleteControl();
        }
        private void LvControls_DoubleClick(object sender, EventArgs e)
        {
            ShowControl();
        }
        private void BtnDown_Click(object sender, EventArgs e)
        {
            int totalItem = lvControls.Items.Count;
            List<ListViewItem> selectedItems = lvControls.SelectedItems.Cast<ListViewItem>().ToList();
            if (selectedItems.Count == 0)
                return;

            ListViewItem firstItem = selectedItems.First();
            ListViewItem lastItem = selectedItems.Last();

            if (lastItem.Index == totalItem - 1)
                return;

            Dictionary<int, ListViewItem> items = new Dictionary<int, ListViewItem>();

            for (int i = firstItem.Index; i < totalItem; i++)
            {
                ListViewItem item = lvControls.Items[i];
                items.Add(item.Index, item);
            }

            for (int i = items.Count - 1; i >= 0; i--)
            {
                lvControls.Items.RemoveAt(items.ElementAt(i).Key);
            }

            List<ListViewItem> temps = new List<ListViewItem>();
            foreach (var item in items)
            {
                if (item.Value.Selected)
                {
                    temps.Add(item.Value);
                }
                else
                {
                    lvControls.Items.Add(item.Value);
                    lvControls.Items.AddRange(temps.ToArray());
                    temps.Clear();
                }
            }
            lvControls.Items.AddRange(temps.ToArray());
        }
        private void BtnUp_Click(object sender, EventArgs e)
        {
            int totalItem = lvControls.Items.Count;
            List<ListViewItem> selectedItems = lvControls.SelectedItems.Cast<ListViewItem>().ToList();
            if (selectedItems.Count == 0)
                return;

            ListViewItem firstItem = selectedItems.First();
            ListViewItem lastItem = selectedItems.Last();

            if (firstItem.Index == 0)
                return;

            Dictionary<int, ListViewItem> items = new Dictionary<int, ListViewItem>();

            for (int i = firstItem.Index - 1; i >= 0 && i < totalItem; i++)
            {
                ListViewItem item = lvControls.Items[i];
                items.Add(item.Index, item);
            }

            for (int i = items.Count - 1; i >= 0; i--)
            {
                lvControls.Items.RemoveAt(items.ElementAt(i).Key);
            }

            List<ListViewItem> temps = new List<ListViewItem>();
            foreach (var item in items)
            {
                if (item.Value.Selected)
                {
                    lvControls.Items.Add(item.Value);
                    lvControls.Items.AddRange(temps.ToArray());
                    temps.Clear();
                }
                else
                {
                    temps.Add(item.Value);
                }
            }
            lvControls.Items.AddRange(temps.ToArray());
        }
        private void BtnNew_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(fileName) && this.showConfirm(Caption: "File is opened. Save it?"))
                SaveData(fileName);

            LoadData(string.Empty);
        }
        private void BtnStop_Click(object sender, EventArgs e)
        {
            Abort();
        }
        private void TbarDelay_ValueChanged(object sender, EventArgs e)
        {
            Define.Instance.DelayValue = tbarDelay.Value;
            delay = tbarDelay.Value;
        }

        void InitEvent()
        {
            //Disable
            btnRun.Click -= btnRun_Click;
            btnLoad.Click -= BtnLoad_Click;
            btnSave.Click -= BtnSave_Click;
            btnAdd.Click -= BtnAdd_Click;
            lvControls.MouseDown -= LvControls_MouseDown;
            lvControls.DoubleClick -= LvControls_DoubleClick;
            cMenu_Delete.Click -= CMenu_Delete_Click;
            btnUp.Click -= BtnUp_Click;
            btnDown.Click -= BtnDown_Click;
            btnNew.Click -= BtnNew_Click;
            btnStop.Click -= BtnStop_Click;
            tbarDelay.ValueChanged -= TbarDelay_ValueChanged;

            //Enable
            btnRun.Click += btnRun_Click;
            btnLoad.Click += BtnLoad_Click;
            btnSave.Click += BtnSave_Click;
            btnAdd.Click += BtnAdd_Click;
            lvControls.MouseDown += LvControls_MouseDown;
            lvControls.DoubleClick += LvControls_DoubleClick;
            cMenu_Delete.Click += CMenu_Delete_Click;
            btnUp.Click += BtnUp_Click;
            btnDown.Click += BtnDown_Click;
            btnNew.Click += BtnNew_Click;
            btnStop.Click += BtnStop_Click;
            tbarDelay.ValueChanged += TbarDelay_ValueChanged;
        }
        void LoadFile()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = ".xml|*.xml";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                LoadData(dialog.FileName);
                btnRun.Select();
            }
        }
        void SaveFile()
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = ".xml|*.xml";
                dialog.FileName = $"{DateTime.Now.ToString("yyyyMMdd-hhmmss-ttt")}";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    Text = dialog.FileName;
                    uc_Comment.SetMessage(Path.GetFileNameWithoutExtension(dialog.FileName), uc_Comment.GetComment());
                    SaveData(dialog.FileName);
                    btnRun.Select();
                }
            }
            else
            {
                SaveData(fileName);
                btnRun.Select();
            }
        }
        void LoadData(string fileName)
        {
            id = 0;
            this.fileName = fileName;
            lvControls.Items.Clear();
            pnControl.Controls.Clear();
            uc_Comment.SetMessage(string.Empty, string.Empty);

            if (!File.Exists(fileName))
                return;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(fileName);

            XmlNode xmlStep = xmlDoc.SelectSingleNode($"/{Define.Instance.Step}");
            if (xmlStep == null)
                return;

            uc_Comment.SetMessage(Path.GetFileNameWithoutExtension(fileName), uc_Comment.GetComment());
            if (xmlStep.Attributes[Define.Instance.Comment] != null)
                uc_Comment.SetMessage(uc_Comment.GetTitle(), xmlStep.Attributes[Define.Instance.Comment].Value);

            /*Load XML*/
            XmlNodeList xmlControls = xmlStep.SelectNodes($"{Define.Instance.Input}");

            foreach (XmlNode xmlControl in xmlControls)
            {
                string type = string.Empty;

                if (xmlControl.Attributes[Define.Instance.Type] != null)
                    type = xmlControl.Attributes[Define.Instance.Type].Value;

                CustomMethod handle = handles.FirstOrDefault(x => x.Name.ToEqualEx(type));
                if (handle == null)
                    continue;

                handle.ExecuteMethod(handle.AddControl, this, new object[] { null, xmlControl });
            }
        }
        void SaveData(string fileName)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();

                this.fileName = fileName;
                if (File.Exists(fileName))
                    xmlDoc.Load(fileName);
                else
                    xmlDoc.InitDocument();

                XmlNode xmlStep = xmlDoc.SelectSingleNode($"/{Define.Instance.Step}");
                if (xmlStep == null)
                    return;

                xmlStep.RemoveAll();

                xmlStep.Attributes.Append(xmlDoc.CreateAttribute(Define.Instance.Title));
                xmlStep.Attributes.Append(xmlDoc.CreateAttribute(Define.Instance.Comment));
                //xmlStep.Attributes[Define.Instance.Title].Value = uc_Comment.GetTitle();
                xmlStep.Attributes[Define.Instance.Comment].Value = uc_Comment.GetComment();

                /*Save XML*/
                foreach (ListViewItem lvItem in lvControls.Items)
                {
                    ControlData cd = lvItem.Tag as ControlData;
                    if (cd == null)
                        continue;

                    Control ctrControl = cd.Control;
                    CustomMethod handle = handles.FirstOrDefault(x => x.Name.ToEqualEx(cd.Type));
                    if (handle == null)
                        continue;

                    handle.ExecuteMethod(handle.SaveData, ctrControl, new object[] { xmlDoc, xmlStep });
                }

                xmlDoc.Save(fileName);
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                this.showError(Caption: ex.Message);
            }
        }
        async void Start()
        {
            int totalItem = lvControls.Items.Count;
            List<ListViewItem> selectedItems = lvControls.GetSelectedItems();

            for (int i = 0; i < totalItem; i++)
            {
                ListViewItem lvItem = lvControls.Items[i];
                lvItem.ForeColor = Define.Instance.clrNormal;
            }


            if (selectedItems.Count == 0)
            {
            }
            else
            {
                DisableControl();
                ChromeHandle.StartChromeDriver();

                steps.Clear();
                foreach (ListViewItem item in selectedItems)
                {
                    item.Selected = false;
                    item.ForeColor = Define.Instance.clrNormal;

                    ControlData cd = item.Tag as ControlData;
                    if (cd == null)
                        continue;

                    CustomMethod handle = handles.FirstOrDefault(x => x.Name.ToEqualEx(cd.Type));
                    if (handle == null)
                        continue;

                    handle.ExecuteMethod(handle.GetControl, this, new object[] { cd.Control, item.Index });
                }

                foreach (Step step in steps)
                {
                    ListViewItem lvItem = lvControls.Items[step.Index];
                    lvItem.ForeColor = Define.Instance.clrRunning;

                    //Wait
                    await Task.Delay(delay);

                    //Close after click stop or close window
                    if (iStatus == Define.eStatus.Abort)
                    {
                        break;
                    }

                    //Run step
                    string result = await Task.Factory.StartNew(() =>
                    {
                        Log.Debug(MethodBase.GetCurrentMethod(), step.Text);
                        return step.Func();
                    });

                    //Set log status
                    if (!string.IsNullOrWhiteSpace(result))
                    {
                        lvItem.ForeColor = Define.Instance.clrFail;
                        break;
                    }
                    else
                    {
                        lvItem.ForeColor = Define.Instance.clrPass;
                    }
                }

                Stop();
            }
        }
        public void Stop()
        {
            EnableControl();
        }
        public void Abort()
        {
            iStatus = Define.eStatus.Abort;
        }
        void EnableControl()
        {
            btnUp.Enabled = true;
            btnDown.Enabled = true;
            cbbCommand.Enabled = true;
            btnAdd.Enabled = true;
            //lvControls.Enabled = true;
            btnNew.Enabled = true;
            btnLoad.Enabled = true;
            btnSave.Enabled = true;
            btnRun.Enabled = true;
            btnStop.Enabled = false;
            cMenu_Delete.Visible = true;
            iStatus = Define.eStatus.Stop;

            lvControls.DoubleClick += LvControls_DoubleClick;
            lvControls.MouseDown += LvControls_MouseDown;
        }
        void DisableControl()
        {
            btnUp.Enabled = false;
            btnDown.Enabled = false;
            cbbCommand.Enabled = false;
            btnAdd.Enabled = false;
            //lvControls.Enabled = false;
            btnNew.Enabled = false;
            btnLoad.Enabled = false;
            btnSave.Enabled = false;
            btnRun.Enabled = false;
            btnStop.Enabled = true;
            cMenu_Delete.Visible = false;
            iStatus = Define.eStatus.Start;

            lvControls.DoubleClick -= LvControls_DoubleClick;
            lvControls.MouseDown -= LvControls_MouseDown;
        }
        void AddControl(UC_MouseHandle uc, XmlNode xmlControl)
        {
            id++;
            uc = new UC_MouseHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Mouse_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Mouse, Control = uc });
        }
        void AddControl(UC_InputHandle uc, XmlNode xmlControl)
        {
            id++;
            uc = new UC_InputHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Input_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Input, Control = uc });
        }
        void AddControl(UC_KeyHandle uc, XmlNode xmlControl)
        {
            id++;
            uc = new UC_KeyHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Key_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Key, Control = uc });
        }
        void AddControl(UC_DisplayHandle uc, XmlNode xmlControl)
        {
            id++;
            uc = new UC_DisplayHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Display_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Display, Control = uc });
        }
        void AddControl(UC_UrlHandle uc, XmlNode xmlControl)
        {
            id++;
            uc = new UC_UrlHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Url_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Url, Control = uc });
        }
        void AddControl(UC_UploadFileHandle uc, XmlNode xmlControl)
        {
            id++;
            uc = new UC_UploadFileHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Upload_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Upload, Control = uc });
        }
        void AddControl(UC_AlertHandle uc, XmlNode xmlControl)
        {
            id++;
            uc = new UC_AlertHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Alert_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Alert, Control = uc });
        }
        void AddControl(UC_PropertiesHandle uc, XmlNode xmlControl)
        {
            id++;
            uc = new UC_PropertiesHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Properties_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Properties, Control = uc });
        }
        void AddControl(UC_PopupHandle uc, XmlNode xmlControl)
        {
            id++;
            uc = new UC_PopupHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Popup_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Popup, Control = uc });
        }
        void AddControl(UC_ReadFilesHandle uc, XmlNode xmlControl)
        {
            id++;
            uc = new UC_ReadFilesHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.ReadFile_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.ReadFile, Control = uc });
        }
        void AddControl(UC_ScrollHandle uc, XmlNode xmlControl)
        {
            id++;
            uc = new UC_ScrollHandle(xmlControl);
            uc.Name = $"{name}{id}";
            uc.Text = $"{Define.Instance.Scroll_Text} {id}";
            uc.Dock = DockStyle.Fill;
            uc.SetTitle(uc.Text);
            uc.Show();

            AddControlData(new ControlData() { Name = uc.Name, Text = uc.Text, Type = Define.Instance.Scroll, Control = uc });
        }
        void GetControl(UC_MouseHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_InputHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_KeyHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_DisplayHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_UrlHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_UploadFileHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_AlertHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_PropertiesHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_PopupHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_ReadFilesHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void GetControl(UC_ScrollHandle uc, int index)
        {
            Step Step = new Step();
            Step.Name = uc.Name;
            Step.Text = uc.Text;
            Step.Func = uc.GetFunc();
            Step.Index = index;
            steps.Add(Step);
        }
        void AddControlData(ControlData cd)
        {
            ListViewItem lvItem = new ListViewItem(new string[] { cd.Text });
            lvItem.Selected = true;
            lvItem.Tag = cd;
            lvControls.Items.Add(lvItem);
        }
        void EditControl(Control control)
        {
            if (!pnControl.Controls.Contains(control))
                pnControl.Controls.Add(control);
            control.BringToFront();
        }
        void ShowControl()
        {
            if (lvControls.SelectedItems.Count != 1)
                return;

            ControlData cd = lvControls.SelectedItems[0].Tag as ControlData;
            if (cd == null)
                return;

            EditControl(cd.Control);
        }
        void DeleteControl()
        {
            for (int i = lvControls.SelectedItems.Count - 1; i >= 0; i--)
            {
                ListViewItem lvItem = lvControls.SelectedItems[i];
                ControlData cd = lvItem.Tag as ControlData;
                if (pnControl.Controls[cd.Name] != null)
                    pnControl.Controls.RemoveByKey(cd.Name);
                lvControls.Items.Remove(lvItem);
            }
        }
        public Define.eStatus GetStatus()
        {
            return iStatus;
        }
        public override void ClearData()
        {
            lvControls.Items.Clear();
            pnControl.Controls.Clear();

            handles = null;
            steps = null;

            GC.Collect();
        }
    }
}
