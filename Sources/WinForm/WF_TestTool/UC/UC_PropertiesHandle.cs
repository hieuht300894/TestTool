﻿using Common;
using SeleniumAPI;
using System;
using System.Collections.Generic;
using System.Xml;

namespace WF_TestTool.UC
{
    public partial class UC_PropertiesHandle : UC_Base
    {
        public UC_PropertiesHandle(XmlNode node)
        {
            InitializeComponent();
            LoadData(node);
        }

        protected override void UC_Base_Load(object sender, EventArgs e)
        {
            chkDisplayed.CheckedChanged -= ChkDisplayed_CheckedChanged;
            chkEnabled.CheckedChanged -= ChkEnabled_CheckedChanged;
            chkLocation.CheckedChanged -= ChkLocation_CheckedChanged;
            chkSelected.CheckedChanged -= ChkSelected_CheckedChanged;
            chkSize.CheckedChanged -= ChkSize_CheckedChanged;

            chkDisplayed.CheckedChanged += ChkDisplayed_CheckedChanged;
            chkEnabled.CheckedChanged += ChkEnabled_CheckedChanged;
            chkLocation.CheckedChanged += ChkLocation_CheckedChanged;
            chkSelected.CheckedChanged += ChkSelected_CheckedChanged;
            chkSize.CheckedChanged += ChkSize_CheckedChanged;
        }

        private void ChkSize_CheckedChanged(object sender, EventArgs e)
        {
            numSize_Width.Enabled = chkSize.Checked;
            numSize_Height.Enabled = chkSize.Checked;
        }
        private void ChkSelected_CheckedChanged(object sender, EventArgs e)
        {
            rbSelected_Yes.Enabled = chkSelected.Checked;
            rbSelected_No.Enabled = chkSelected.Checked;
        }
        private void ChkLocation_CheckedChanged(object sender, EventArgs e)
        {
            numLocation_X.Enabled = chkLocation.Checked;
            numLocation_Y.Enabled = chkLocation.Checked;
        }
        private void ChkEnabled_CheckedChanged(object sender, EventArgs e)
        {
            rbEnabled_Yes.Enabled = chkEnabled.Checked;
            rbEnabled_No.Enabled = chkEnabled.Checked;
        }
        private void ChkDisplayed_CheckedChanged(object sender, EventArgs e)
        {
            rbDisplayed_Yes.Enabled = chkDisplayed.Checked;
            rbDisplayed_No.Enabled = chkDisplayed.Checked;
        }

        public void GetData(PropertiesObject obj)
        {
            obj.bDisplayed = chkDisplayed.Checked;
            obj.bEnabled = chkEnabled.Checked;
            obj.bSelected = chkSelected.Checked;
            obj.bLocation = chkLocation.Checked;
            obj.bSize = chkSize.Checked;

            obj.bDisplayed_Checked = rbDisplayed_Yes.Checked;
            obj.bEnabled_Checked = rbEnabled_Yes.Checked;
            obj.bSelected_Checked = rbSelected_Yes.Checked;
            obj.iLocation_X = chkLocation.Checked ? Convert.ToInt32(numLocation_X.Value) : 0;
            obj.iLocation_Y = chkLocation.Checked ? Convert.ToInt32(numLocation_Y.Value) : 0;
            obj.iSize_Width = chkSize.Checked ? Convert.ToInt32(numSize_Width.Value) : 0;
            obj.iSize_Height = chkSize.Checked ? Convert.ToInt32(numSize_Height.Value) : 0;

            obj.sTarget = txtTarget.Text.Trim();
            obj.iTimeout = Convert.ToInt32(numTimeout.Value);
        }
        public override Func<string> GetFunc()
        {
            PropertiesObject obj = new PropertiesObject();
            GetData(obj);

            return PropertiesHandle.GetFunc(obj);
        }
        public override void SetTitle(string title)
        {
            lbTitle.Text = title;
        }
        protected override void LoadData(XmlNode xmlInput = null)
        {
            numLocation_X.Format();
            numLocation_Y.Format();
            numSize_Height.Format();
            numSize_Width.Format();
            numTimeout.Format();
            txtTarget.Format();

            bool bDisplayed = false;
            bool bEnabled = false;
            bool bSelected = false;
            bool bLocation = false;
            bool bSize = false;
            bool bDisplayed_Checked = false;
            bool bEnabled_Checked = false;
            bool bSelected_Checked = false;
            int iLocation_X = 0;
            int iLocation_Y = 0;
            int iSize_Width = 0;
            int iSize_Height = 0;
            string sTarget = string.Empty;
            int iTimeout = Define.Instance.TimeoutValue;

            if (xmlInput != null)
            {
                XmlNodeList xmlProperties = xmlInput.SelectNodes(Define.Instance.Property);
                foreach (XmlNode xmlProperty in xmlProperties)
                {
                    string sName = string.Empty;

                    if (xmlProperty.Attributes[Define.Instance.Name] != null)
                        sName = xmlProperty.Attributes[Define.Instance.Name].Value;

                    if (sName.ToEqualEx(Define.Instance.Displayed))
                    {
                        if (xmlProperty.Attributes[Define.Instance.Value] != null)
                        {
                            bool.TryParse(xmlProperty.Attributes[Define.Instance.Value].Value, out bDisplayed_Checked);
                            bDisplayed = true;
                        }
                    }
                    else if (sName.ToEqualEx(Define.Instance.Enabled))
                    {
                        if (xmlProperty.Attributes[Define.Instance.Value] != null)
                        {
                            bool.TryParse(xmlProperty.Attributes[Define.Instance.Value].Value, out bEnabled_Checked);
                            bEnabled = true;
                        }
                    }
                    else if (sName.ToEqualEx(Define.Instance.Selected))
                    {
                        if (xmlProperty.Attributes[Define.Instance.Value] != null)
                        {
                            bool.TryParse(xmlProperty.Attributes[Define.Instance.Value].Value, out bSelected_Checked);
                            bSelected = true;
                        }
                    }
                    else if (sName.ToEqualEx(Define.Instance.Location))
                    {
                        if (xmlProperty.Attributes[Define.Instance.PositionX] != null && xmlProperty.Attributes[Define.Instance.PositionY] != null)
                        {
                            int.TryParse(xmlProperty.Attributes[Define.Instance.PositionX].Value, out iLocation_X);
                            int.TryParse(xmlProperty.Attributes[Define.Instance.PositionY].Value, out iLocation_Y);
                            bLocation = true;
                        }
                    }
                    else if (sName.ToEqualEx(Define.Instance.Size))
                    {
                        if (xmlProperty.Attributes[Define.Instance.Width] != null && xmlProperty.Attributes[Define.Instance.Height] != null)
                        {
                            int.TryParse(xmlProperty.Attributes[Define.Instance.Width].Value, out iSize_Width);
                            int.TryParse(xmlProperty.Attributes[Define.Instance.Height].Value, out iSize_Height);
                            bSize = true;
                        }
                    }
                }
                if (xmlInput.Attributes[Define.Instance.Target] != null)
                    sTarget = xmlInput.Attributes[Define.Instance.Target].Value;
                if (xmlInput.Attributes[Define.Instance.Timeout] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.Timeout].Value, out iTimeout);
            }

            chkDisplayed.Checked = bDisplayed;
            chkEnabled.Checked = bEnabled;
            chkSelected.Checked = bSelected;
            chkLocation.Checked = bLocation;
            chkSize.Checked = bSize;
            rbDisplayed_Yes.Checked = bDisplayed_Checked;
            rbDisplayed_No.Checked = !bDisplayed_Checked;
            rbEnabled_Yes.Checked = bEnabled_Checked;
            rbEnabled_No.Checked = !bEnabled_Checked;
            rbSelected_Yes.Checked = bSelected_Checked;
            rbSelected_No.Checked = !bSelected_Checked;
            numLocation_X.Value = iLocation_X;
            numLocation_Y.Value = iLocation_Y;
            numSize_Width.Value = iSize_Width;
            numSize_Height.Value = iSize_Height;
            txtTarget.Text = sTarget;
            numTimeout.Value = iTimeout;

            ChkDisplayed_CheckedChanged(chkDisplayed, new EventArgs());
            ChkEnabled_CheckedChanged(chkEnabled, new EventArgs());
            ChkSelected_CheckedChanged(chkSelected, new EventArgs());
            ChkLocation_CheckedChanged(chkLocation, new EventArgs());
            ChkSize_CheckedChanged(chkSize, new EventArgs());
        }
        public override void SaveData(XmlDocument xmlDoc, XmlNode xmlStep)
        {
            PropertiesObject obj = new PropertiesObject();
            GetData(obj);

            Dictionary<string, object> attrs = new Dictionary<string, object>();
            attrs.Add(Define.Instance.Type, Define.Instance.Properties);
            attrs.Add(Define.Instance.Target, obj.sTarget);
            attrs.Add(Define.Instance.Timeout, obj.iTimeout.ToString());
            XmlNode xmlInput = xmlDoc.AppendChildEx(xmlStep, Define.Instance.Input, attrs);

            if (obj.bDisplayed)
            {
                Dictionary<string, object> attrKeys = new Dictionary<string, object>();
                attrKeys.Add(Define.Instance.Name, Define.Instance.Displayed);
                attrKeys.Add(Define.Instance.Value, obj.bDisplayed_Checked.ToString());
                xmlDoc.AppendChildEx(xmlInput, Define.Instance.Property, attrKeys);
            }
            if (obj.bEnabled)
            {
                Dictionary<string, object> attrKeys = new Dictionary<string, object>();
                attrKeys.Add(Define.Instance.Name, Define.Instance.Enabled);
                attrKeys.Add(Define.Instance.Value, obj.bEnabled_Checked.ToString());
                xmlDoc.AppendChildEx(xmlInput, Define.Instance.Property, attrKeys);
            }
            if (obj.bSelected)
            {
                Dictionary<string, object> attrKeys = new Dictionary<string, object>();
                attrKeys.Add(Define.Instance.Name, Define.Instance.Selected);
                attrKeys.Add(Define.Instance.Value, obj.bSelected_Checked.ToString());
                xmlDoc.AppendChildEx(xmlInput, Define.Instance.Property, attrKeys);
            }
            if (obj.bLocation)
            {
                Dictionary<string, object> attrKeys = new Dictionary<string, object>();
                attrKeys.Add(Define.Instance.Name, Define.Instance.Location);
                attrKeys.Add(Define.Instance.PositionX, obj.iLocation_X.ToString());
                attrKeys.Add(Define.Instance.PositionY, obj.iLocation_Y.ToString());
                xmlDoc.AppendChildEx(xmlInput, Define.Instance.Property, attrKeys);
            }
            if (obj.bSize)
            {
                Dictionary<string, object> attrKeys = new Dictionary<string, object>();
                attrKeys.Add(Define.Instance.Name, Define.Instance.Size);
                attrKeys.Add(Define.Instance.Width, obj.iSize_Width.ToString());
                attrKeys.Add(Define.Instance.Height, obj.iSize_Height.ToString());
                xmlDoc.AppendChildEx(xmlInput, Define.Instance.Property, attrKeys);
            }
        }
    }
}
