﻿using Common;
using SeleniumAPI;
using System;
using System.Collections.Generic;
using System.Xml;

namespace WF_TestTool.UC
{
    public partial class UC_UrlHandle : UC_Base
    {
        public UC_UrlHandle(XmlNode node)
        {
            InitializeComponent();
            LoadData(node);
        }

        protected override void UC_Base_Load(object sender, EventArgs e)
        {
        }

        public void GetData(UrlObject obj)
        {
            obj.sUrl = txtUrl.Text.Trim();
            obj.iTimeout = Convert.ToInt32(numTimeout.Value);
        }
        public override Func<string> GetFunc()
        {
            UrlObject obj = new UrlObject();
            GetData(obj);

            return UrlHandle.GetFunc(obj);
        }
        public override void SetTitle(string title)
        {
            lbTitle.Text = title;
        }
        protected override void LoadData(XmlNode xmlInput = null)
        {
            txtUrl.Format();
            numTimeout.Format();

            string sUrl = string.Empty;
            int iTimeout = Define.Instance.TimeoutValue;

            if (xmlInput != null)
            {
                if (xmlInput.Attributes[Define.Instance.Url] != null)
                    sUrl = xmlInput.Attributes[Define.Instance.Url].Value;
                if (xmlInput.Attributes[Define.Instance.Timeout] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.Timeout].Value, out iTimeout);
            }

            txtUrl.Text = sUrl;
            numTimeout.Value = iTimeout;
        }
        public override void SaveData(XmlDocument xmlDoc, XmlNode xmlStep)
        {
            UrlObject obj = new UrlObject();
            GetData(obj);

            Dictionary<string, object> attrs = new Dictionary<string, object>();
            attrs.Add(Define.Instance.Type, Define.Instance.Url);
            attrs.Add(Define.Instance.Url, obj.sUrl);
            attrs.Add(Define.Instance.Timeout, obj.iTimeout);
            xmlDoc.AppendChildEx(xmlStep, Define.Instance.Input, attrs);
        }
    }
}
