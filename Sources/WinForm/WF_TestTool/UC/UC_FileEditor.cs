﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Folding;
using System.Windows;
using Common;
using System.Reflection;

namespace WF_TestTool.UC
{
    public partial class UC_FileEditor : UC_Base
    {
        public delegate void SendText(string text);
        public SendText sendText;

        TextEditor editor;
        ElementHost host;
        FoldingManager foldingManager;
        XmlFoldingStrategy foldingStrategy;
        Timer timer;
        string path = string.Empty;

        public UC_FileEditor()
        {
            InitializeComponent();

            editor = new TextEditor();
            host = new ElementHost();
            foldingManager = FoldingManager.Install(editor.TextArea);
            foldingStrategy = new XmlFoldingStrategy();
            timer = new Timer() { Interval = 1000 };
        }
        public UC_FileEditor(string path)
        {
            InitializeComponent();

            editor = new TextEditor();
            host = new ElementHost();
            foldingManager = FoldingManager.Install(editor.TextArea);
            foldingStrategy = new XmlFoldingStrategy();
            timer = new Timer() { Interval = 1000 };
            this.path = path;
        }

        protected override void UC_Base_Load(object sender, EventArgs e)
        {
            OpenEditor();
            InitEvent();
        }
        protected override void UC_Base_SizeChanged(object sender, EventArgs e)
        {
            editor.Options.HighlightCurrentLine = false;
            editor.Options.HighlightCurrentLine = true;
        }

        private void Editor_TextChanged(object sender, EventArgs e)
        {
            timer.Stop();
            timer.Start();
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            timer.Tick -= Timer_Tick;

            timer.Stop();
            foldingStrategy.UpdateFoldings(foldingManager, editor.Document);

            timer.Tick += Timer_Tick;
        }
        private void BtnOK_Click(object sender, EventArgs e)
        {
            sendText?.Invoke(editor.Text);
        }

        void OpenEditor()
        {
            try
            {
                string text = string.Empty;
                Extension.ReadText(path, ref text);
                editor.Text = text;

                host.Name = "ElementHost";
                host.Dock = DockStyle.Fill;
                host.Child = editor;
                pnEditor.Controls.Add(host);

                editor.SyntaxHighlighting = HighlightingManager.Instance.GetDefinition("XML");
                editor.FontFamily = new System.Windows.Media.FontFamily("Consolas");
                editor.FontSize = 13.25;
                editor.FontWeight = FontWeights.Regular;
                editor.ShowLineNumbers = true;
                editor.Options.HighlightCurrentLine = true;

                foldingStrategy.UpdateFoldings(foldingManager, editor.Document);
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
            }
        }
        void InitEvent()
        {
            timer.Tick -= Timer_Tick;
            editor.TextChanged -= Editor_TextChanged;
            btnOK.Click -= BtnOK_Click;

            timer.Tick += Timer_Tick;
            editor.TextChanged += Editor_TextChanged;
            btnOK.Click += BtnOK_Click;
        }
        void CloseEditor()
        {
        }
    }
}
