﻿using Common;
using SeleniumAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;

namespace WF_TestTool.UC
{
    public partial class UC_KeyHandle : UC_Base
    {
        BindingList<KeyData> keyDatas = new BindingList<KeyData>();
        /// <summary>
        /// 0: keyPress; 1: keyDown or keyUp
        /// </summary>
        int lastChecked = 0;

        public UC_KeyHandle(XmlNode node)
        {
            InitializeComponent();
            LoadData(node);
        }

        protected override void UC_Base_Load(object sender, EventArgs e)
        {
            chkIsPosition.CheckedChanged -= ChkIsPosition_CheckedChanged;
            chkIsPosition.CheckedChanged += ChkIsPosition_CheckedChanged;

            rbtnIsKeyPress.CheckedChanged -= RbtnIsKeyPress_CheckedChanged;
            rbtnIsKeyPress.CheckedChanged += RbtnIsKeyPress_CheckedChanged;

            rbtnIsKeyDown.CheckedChanged -= RbtnIsKeyDownUp_CheckedChanged;
            rbtnIsKeyDown.CheckedChanged += RbtnIsKeyDownUp_CheckedChanged;

            rbtnIsKeyUp.CheckedChanged -= RbtnIsKeyDownUp_CheckedChanged;
            rbtnIsKeyUp.CheckedChanged += RbtnIsKeyDownUp_CheckedChanged;
        }
        private void RbtnIsKeyDownUp_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbtn = sender as RadioButton;
            if (lastChecked == 0 && rbtn.Checked)
            {
                keyDatas.Clear();
                KeyData.GetModifierKeys().ForEach(x => keyDatas.Add(x));
                lastChecked = 1;
            }
        }
        private void RbtnIsKeyPress_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbtn = sender as RadioButton;
            if (lastChecked == 1 && rbtn.Checked)
            {
                keyDatas.Clear();
                KeyData.GetOtherKeys().ForEach(x => keyDatas.Add(x));
                lastChecked = 0;
            }
        }
        private void ChkIsPosition_CheckedChanged(object sender, EventArgs e)
        {
            numX.Enabled = chkIsPosition.Checked;
            numY.Enabled = chkIsPosition.Checked;
        }

        public void GetData(KeyboardObject obj)
        {
            if (rbtnIsKeyPress.Checked)
                obj.iKeyType = 0;
            if (rbtnIsKeyDown.Checked)
                obj.iKeyType = 1;
            if (rbtnIsKeyUp.Checked)
                obj.iKeyType = 2;
            obj.sKey = cbbKey.SelectedValue != null ? cbbKey.SelectedValue.ToString() : string.Empty;
            obj.sTarget = txtTarget.Text.Trim();
            obj.bPosition = chkIsPosition.Checked;
            obj.iX = chkIsPosition.Checked ? Convert.ToInt32(numX.Value) : 0;
            obj.iY = chkIsPosition.Checked ? Convert.ToInt32(numY.Value) : 0;
            obj.iTimeout = Convert.ToInt32(numTimeout.Value);
        }
        public override Func<string> GetFunc()
        {
            KeyboardObject obj = new KeyboardObject();
            GetData(obj);

            return KeyboardHandle.GetFunc(obj);
        }
        public override void SetTitle(string title)
        {
            lbTitle.Text = title;
        }
        protected override void LoadData(XmlNode xmlInput = null)
        {
            cbbKey.DataSource = keyDatas;

            txtTarget.Format();
            numTimeout.Format();
            numX.Format();
            numY.Format();
            cbbKey.FormatEx("Value", "Name");

            int iKeyType = 0;
            string sKey = string.Empty;
            string sTarget = string.Empty;
            bool bPosition = false;
            int iX = 0;
            int iY = 0;
            int iTimeout = Define.Instance.TimeoutValue;

            if (xmlInput != null)
            {
                if (xmlInput.Attributes[Define.Instance.KeyType] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.KeyType].Value, out iKeyType);
                if (xmlInput.Attributes[Define.Instance.Value] != null)
                    sKey = xmlInput.Attributes[Define.Instance.Value].Value;
                if (xmlInput.Attributes[Define.Instance.Target] != null)
                    sTarget = xmlInput.Attributes[Define.Instance.Target].Value;
                if (xmlInput.Attributes[Define.Instance.IsPosition] != null)
                    bool.TryParse(xmlInput.Attributes[Define.Instance.IsPosition].Value, out bPosition);
                if (xmlInput.Attributes[Define.Instance.PositionX] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.PositionX].Value, out iX);
                if (xmlInput.Attributes[Define.Instance.PositionY] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.PositionY].Value, out iY);
                if (xmlInput.Attributes[Define.Instance.Timeout] != null)
                    int.TryParse(xmlInput.Attributes[Define.Instance.Timeout].Value, out iTimeout);
            }

            if (iKeyType == 1)
            {
                keyDatas.Clear();
                KeyData.GetModifierKeys().ForEach(x => keyDatas.Add(x));
                lastChecked = 1;
                rbtnIsKeyDown.Checked = true;
            }
            else if (iKeyType == 2)
            {
                keyDatas.Clear();
                KeyData.GetModifierKeys().ForEach(x => keyDatas.Add(x));
                lastChecked = 1;
                rbtnIsKeyUp.Checked = true;
            }
            else
            {
                keyDatas.Clear();
                KeyData.GetOtherKeys().ForEach(x => keyDatas.Add(x));
                lastChecked = 0;
                rbtnIsKeyPress.Checked = true;
            }

            cbbKey.SelectedValue = sKey;
            txtTarget.Text = sTarget;
            chkIsPosition.Checked = bPosition;
            numX.Value = iX;
            numY.Value = iY;
            numTimeout.Value = iTimeout;

            ChkIsPosition_CheckedChanged(chkIsPosition, new EventArgs());
        }
        public override void SaveData(XmlDocument xmlDoc, XmlNode xmlStep)
        {
            KeyboardObject obj = new KeyboardObject();
            GetData(obj);

            Dictionary<string, object> attrs = new Dictionary<string, object>();
            attrs.Add(Define.Instance.Type, Define.Instance.Key);
            attrs.Add(Define.Instance.KeyType, obj.iKeyType);
            attrs.Add(Define.Instance.Value, obj.sKey);
            attrs.Add(Define.Instance.Target, obj.sTarget);
            if (obj.bPosition)
            {
                attrs.Add(Define.Instance.IsPosition, obj.bPosition);
                attrs.Add(Define.Instance.PositionX, obj.iX);
                attrs.Add(Define.Instance.PositionY, obj.iY);
            }
            attrs.Add(Define.Instance.Timeout, obj.iTimeout);
            xmlDoc.AppendChildEx(xmlStep, Define.Instance.Input, attrs);
        }
    }
}