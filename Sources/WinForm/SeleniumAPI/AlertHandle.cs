﻿using Common;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAPI
{
    public static class AlertHandle
    {
        public static Func<string> GetFunc(AlertObject obj)
        {
            return new Func<string>(() =>
            {
                WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                try
                {
                    wait.Until(wd =>
                    {
                        try
                        {
                            bool chk = true;

                            if (!wd.SwitchTo().Alert().Text.ToEqualEx(obj.sText))
                                chk = false;

                            if (obj.sAlertType == Define.eAlertType.PromptAlert && obj.sInput.IsNotEmpty())
                                wd.SwitchTo().Alert().SendKeys(obj.sInput);

                            if (obj.sButtonType == Define.eAlertType.OK)
                                wd.SwitchTo().Alert().Accept();
                            else if (obj.sButtonType == Define.eAlertType.Cancel)
                                wd.SwitchTo().Alert().Dismiss();

                            return chk;
                        }
                        catch (Exception ex)
                        {
                            Log.Error(MethodBase.GetCurrentMethod(), ex);
                            return false;
                        }
                    });
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    Log.Error(MethodBase.GetCurrentMethod(), ex);
                    return ex.Message;
                }
            });
        }
    }

    public class AlertObject
    {
        public Define.eAlertType sAlertType { get; set; } = Define.eAlertType.None;
        public string sText { get; set; } = string.Empty;
        public string sInput { get; set; } = string.Empty;
        public Define.eAlertType sButtonType { get; set; } = Define.eAlertType.None;
        public int iTimeout { get; set; }= Define.Instance.TimeoutValue;
    }
}
