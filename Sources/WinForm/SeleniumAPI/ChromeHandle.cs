﻿using Common;
using Common.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SeleniumAPI
{
    public static class ChromeHandle
    {
        //Public
        public static string ElementToXPathScript { get; set; } = Extension.ReadText(Environment.CurrentDirectory + @"\script\element-xpath.js");
        public static string WindowRectScript { get; set; } = Extension.ReadText(Environment.CurrentDirectory + @"\script\GetWindowRect.js");
        public static string CurrentWindowHandle { get; set; } = string.Empty;
        public static List<string> WindowHandles { get; set; } = new List<string>();
        public static IWebDriver ChromeDriver { get; private set; } = null;

        //Private
        static ChromeDriverService chromeService { get; set; } = null;
        static Point OuterLocation { get; set; } = new Point();
        static Point InnerLocation { get; set; } = new Point();
        static Size OuterSize { get; set; } = new Size();
        static Size InnerSize { get; set; } = new Size();

        public static IWebDriver GetDriver()
        {
            return ChromeDriver;
        }
        public static void GetWindowRect()
        {
            if (ChromeDriver == null || WindowRectScript.IsEmpty())
                return;

            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"{WindowRectScript}");
            IJavaScriptExecutor js = ChromeDriver as IJavaScriptExecutor;

            try
            {
                Dictionary<string, object> obj = js.ExecuteScript(sbScript.ToString()) as Dictionary<string, object>;
                if (obj != null)
                {
                    OuterLocation = new Point(Convert.ToInt32(obj["OuterLocationX"]), Convert.ToInt32(obj["OuterLocationY"]));
                    OuterSize = new Size(Convert.ToInt32(obj["OuterSizeWidth"]), Convert.ToInt32(obj["OuterSizeHeight"]));
                    InnerLocation = new Point(Convert.ToInt32(obj["InnerLocationX"]), Convert.ToInt32(obj["InnerLocationY"]));
                    InnerSize = new Size(Convert.ToInt32(obj["InnerSizeWidth"]), Convert.ToInt32(obj["InnerSizeHeight"]));
                }
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
            }

        }
        public static DOMRect GetElementRect(this IWebElement element)
        {
            DOMRect rect = new DOMRect();

            if (ChromeDriver == null || element == null)
                return rect;

            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"return arguments[0].getBoundingClientRect();");
            IJavaScriptExecutor js = ChromeDriver as IJavaScriptExecutor;

            try
            {
                Dictionary<string, object> obj = js.ExecuteScript(sbScript.ToString(), element) as Dictionary<string, object>;
                if (obj != null)
                {
                    rect.Bottom = Convert.ToInt32(obj[nameof(rect.Bottom).ToLowerEx()]);
                    rect.Height = Convert.ToInt32(obj[nameof(rect.Height).ToLowerEx()]);
                    rect.Left = Convert.ToInt32(obj[nameof(rect.Left).ToLowerEx()]);
                    rect.Right = Convert.ToInt32(obj[nameof(rect.Right).ToLowerEx()]);
                    rect.Top = Convert.ToInt32(obj[nameof(rect.Top).ToLowerEx()]);
                    rect.Width = Convert.ToInt32(obj[nameof(rect.Width).ToLowerEx()]);
                    rect.X = Convert.ToInt32(obj[nameof(rect.X).ToLowerEx()]);
                    rect.Y = Convert.ToInt32(obj[nameof(rect.Y).ToLowerEx()]);
                }
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
            }

            return rect;
        }
        public static DOMRect GetElementRect(int x, int y)
        {
            DOMRect rect = new DOMRect();

            if (ChromeDriver == null)
                return rect;

            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"{WindowRectScript}");
            sbScript.AppendLine($"var _element = document.elementFromPoint({x} - _window.InnerLocationX, {y} - _window.InnerLocationY);");
            sbScript.AppendLine($"console.log(_element);");
            sbScript.AppendLine($"return _element.getBoundingClientRect();");
            IJavaScriptExecutor js = ChromeDriver as IJavaScriptExecutor;

            try
            {
                Dictionary<string, object> obj = js.ExecuteScript(sbScript.ToString()) as Dictionary<string, object>;
                if (obj != null)
                {
                    rect.Bottom = Convert.ToInt32(obj[nameof(rect.Bottom).ToLowerEx()]);
                    rect.Height = Convert.ToInt32(obj[nameof(rect.Height).ToLowerEx()]);
                    rect.Left = Convert.ToInt32(obj[nameof(rect.Left).ToLowerEx()]);
                    rect.Right = Convert.ToInt32(obj[nameof(rect.Right).ToLowerEx()]);
                    rect.Top = Convert.ToInt32(obj[nameof(rect.Top).ToLowerEx()]);
                    rect.Width = Convert.ToInt32(obj[nameof(rect.Width).ToLowerEx()]);
                    rect.X = Convert.ToInt32(obj[nameof(rect.X).ToLowerEx()]);
                    rect.Y = Convert.ToInt32(obj[nameof(rect.Y).ToLowerEx()]);
                }
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
            }

            return rect;
        }
        /// <summary>
        /// bNewType: True-NewTab, False-NewWindow
        /// </summary>
        /// <param name="webDriver"></param>
        /// <param name="bNewType"></param>
        /// <param name="bFullScreen"></param>
        /// <param name="iWidth"></param>
        /// <param name="iHeight"></param>
        public static void NewWindowHandle(bool bNewType = true, int iWidth = 0, int iHeight = 0)
        {
            if (ChromeDriver == null)
                return;

            StringBuilder sbScript = new StringBuilder();
            if (bNewType)
                sbScript.AppendLine($"window.open();");
            else
                sbScript.AppendLine($"window.open('','','height={iWidth},width={iHeight}');");
            IJavaScriptExecutor js = ChromeDriver as IJavaScriptExecutor;
            js.ExecuteScript(sbScript.ToString());
        }

        public static bool WaitForLoad()
        {
            try
            {
                if (ChromeDriver == null)
                    return false;

                IJavaScriptExecutor js = ChromeDriver as IJavaScriptExecutor;
                if (!js.ExecuteScript("return document.readyState").Equals("complete"))
                    return false;
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool StartChromeDriver()
        {
            try
            {
                if (chromeService == null)
                {
                    chromeService = ChromeDriverService.CreateDefaultService();

                    string val = bool.TrueString;
                    if (Extension.GetAppSettings("add", "key", "enableCommandPromptWindow", "value", ref val))
                    {
                        bool bShow = true;
                        bool.TryParse(val, out bShow);
                        chromeService.HideCommandPromptWindow = !bShow;
                    }
                    val = bool.FalseString;
                    if (Extension.GetAppSettings("add", "key", "enableVerboseLogging", "value", ref val))
                    {
                        bool bShow = false;
                        bool.TryParse(val, out bShow);
                        chromeService.EnableVerboseLogging = bShow;
                    }
                }

                if (ChromeDriver == null)
                {
                    ChromeDriver = new ChromeDriver(chromeService);
                    ChromeDriver.Manage().Window.Maximize();
                    Define.Instance.BrowserProcess = Process.GetProcessesByName("chrome").FirstOrDefault(x => x.MainWindowHandle != IntPtr.Zero && x.MainWindowTitle.ToEqualEx("data:, - Google Chrome"));
                    ChromeDriver = ChromeDriver;
                    WindowHandles.Clear();
                    WindowHandles.Add(ChromeDriver.CurrentWindowHandle);
                    CurrentWindowHandle = ChromeDriver.CurrentWindowHandle;
                }
                else if (Define.Instance.BrowserProcess != null && Define.Instance.BrowserProcess.HasExited)
                {
                    ShutdownChromeDriver();

                    ChromeDriver = new ChromeDriver(chromeService);
                    ChromeDriver.Manage().Window.Maximize();
                    Define.Instance.BrowserProcess = Process.GetProcessesByName("chrome").FirstOrDefault(x => x.MainWindowHandle != IntPtr.Zero && x.MainWindowTitle.ToEqualEx("data:, - Google Chrome"));
                    ChromeDriver = ChromeDriver;
                    WindowHandles.Clear();
                    WindowHandles.Add(ChromeDriver.CurrentWindowHandle);
                    CurrentWindowHandle = ChromeDriver.CurrentWindowHandle;
                }
                else
                {
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return false;
            }
        }
        public static bool ShutdownChromeDriver()
        {
            try
            {
                if (ChromeDriver != null)
                {
                    ChromeDriver.Quit();
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return false;
            }
        }
        public static bool CloseWindowHandles()
        {
            if (ChromeDriver != null)
            {
                //Goto first handle
                ChromeDriver.SwitchTo().Window(ChromeDriver.WindowHandles.First());

                //Create new tab or new window
                NewWindowHandle();

                //List window handles
                List<string> handles = ChromeDriver.WindowHandles.ToList();

                //Get last new tab or new window
                string newTab = handles.Last();

                //Close all tab but except new handle
                handles.Where(x => !x.ToEqualEx(newTab)).ToList().ForEach(x => ChromeDriver.SwitchTo().Window(x).Close());

                //Save last new tab or new window 
                WindowHandles.Clear();
                WindowHandles.Add(newTab);
                CurrentWindowHandle = newTab;

                //Goto new handle
                ChromeDriver.SwitchTo().Window(CurrentWindowHandle);
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool GoToUrl(string url)
        {
            try
            {
                if (ChromeDriver == null)
                    return false;
                ChromeDriver.Navigate().GoToUrl(url);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return false;
            }
        }
        public static bool Screenshot()
        {
            try
            {
                if (ChromeDriver == null)
                    return false;

                if (!Directory.Exists("IMG"))
                    Directory.CreateDirectory("IMG");
                RemoteWebDriver rwd = ChromeDriver as RemoteWebDriver;
                rwd.GetScreenshot().SaveAsFile($"IMG/{DateTime.Now.ToString("yyyyMMdd-HHmmss")}.{ScreenshotImageFormat.Png.ToString()}", ScreenshotImageFormat.Png);

                return true;
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return false;
            }
        }
        public static string GetHTML()
        {
            return ChromeDriver.PageSource;
        }
        public static string GetHTMLBy(string xPath)
        {
            try
            {
                var element = ChromeDriver.FindElement(By.XPath(xPath));
                IJavaScriptExecutor js = ChromeDriver as IJavaScriptExecutor;
                if (js != null)
                    return js.ExecuteScript("return arguments[0].outerHTML;", element)?.ToString();
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
