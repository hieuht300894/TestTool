﻿using Common;
using Common.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAPI
{
    public static class UploadHandle
    {
        public static Func<string> GetFunc(UploadObject obj)
        {
            return new Func<string>(() =>
            {
                WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                try
                {
                    wait.Until(wd =>
                    {
                        IWebElement element = null;
                        try
                        {
                            element = wd.FindElement(By.XPath(obj.sTarget));
                        }
                        catch (Exception ex)
                        {
                            Log.Error(MethodBase.GetCurrentMethod(), ex);
                            return false;
                        }

                        try
                        {
                            DOMRect rect = element.GetElementRect();
                            Log.Debug(MethodBase.GetCurrentMethod(), element.TagName, rect);
                            element.SendKeys(Extension.GetFullPath(obj.sPath));
                            return true;
                        }
                        catch (Exception ex)
                        {
                            Log.Error(MethodBase.GetCurrentMethod(), ex);
                            return false;
                        }
                    });
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    Log.Error(MethodBase.GetCurrentMethod(), ex);
                    return ex.Message;
                }
            });
        }
    }

    public class UploadObject
    {
        public string sTarget { get; set; } = string.Empty;
        public string sPath { get; set; } = string.Empty;
        public int iTimeout { get; set; } = Define.Instance.DelayValue;
    }
}
