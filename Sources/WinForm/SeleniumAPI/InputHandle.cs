﻿using Common;
using Common.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Reflection;

namespace SeleniumAPI
{
    public static class InputHandle
    {
        public static Func<string> GetFunc(InputObject obj)
        {
            if (obj.bPosition)
            {
                return new Func<string>(() =>
                {
                    WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                    try
                    {
                        wait.Until(wd =>
                        {
                            Actions builder = new Actions(wd);
                            IWebElement element = null;
                            try
                            {
                                element = wd.FindElement(By.XPath(obj.sTarget));
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }

                            try
                            {
                                if (element != null)
                                {
                                    DOMRect rect = element.GetElementRect();
                                    Log.Debug(MethodBase.GetCurrentMethod(), element.TagName, rect);

                                    builder.MoveToElement(element, element.Location.X + obj.iX, element.Location.Y + obj.iY).Click(element).SendKeys(obj.sValue).Build().Perform();
                                }
                                else
                                {
                                    builder.MoveByOffset(obj.iX, obj.iY).Click(element).SendKeys(obj.sValue).Build().Perform();
                                }

                                ChromeHandle.Screenshot();
                                return true;
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }
                        });
                        return string.Empty;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), ex);
                        return ex.Message;
                    }
                });
            }
            else
            {
                return new Func<string>(() =>
                {
                    WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                    try
                    {
                        wait.Until(wd =>
                        {
                            Actions builder = new Actions(wd);
                            IWebElement element = null;
                            try
                            {
                                element = wd.FindElement(By.XPath(obj.sTarget));
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }

                            try
                            {
                                DOMRect rect = element.GetElementRect();
                                Log.Debug(MethodBase.GetCurrentMethod(), element.TagName, rect);

                                builder.MoveToElement(element).Click(element).SendKeys(obj.sValue).Build().Perform();
                                ChromeHandle.Screenshot();
                                return true;
                            }
                            catch (Exception ex)
                            {
                                Log.Error(MethodBase.GetCurrentMethod(), ex);
                                return false;
                            }
                        });
                        return string.Empty;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(MethodBase.GetCurrentMethod(), ex);
                        return ex.Message;
                    }
                });
            }
        }
    }

    public class InputObject
    {
        public string sTarget { get; set; } = string.Empty;
        public string sValue { get; set; } = string.Empty;
        public bool bPosition { get; set; } = false;
        public int iX { get; set; } = 0;
        public int iY { get; set; } = 0;
        public int iTimeout { get; set; } = Define.Instance.DelayValue;
    }
}