﻿using Common;
using Common.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Reflection;
using System.Text;

namespace SeleniumAPI
{
    public static class DisplayHandle
    {
        public static string GetDisplay(this IWebDriver webDriver, IWebElement targetElement)
        {
            if (targetElement.TagName.ToEqualEx(Define.Instance.Input))
                return webDriver.GetValue(targetElement);
            else if (targetElement.TagName.ToEqualEx(Define.Instance.Button))
                return webDriver.GetValue(targetElement);
            else
                return webDriver.GetText(targetElement);
        }
        public static string GetValue(this IWebDriver webDriver, IWebElement targetElement)
        {
            //return targetElement.GetAttribute("value");

            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"return arguments[0].value;");

            IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            return (js.ExecuteScript(sbScript.ToString(), targetElement) as string) ?? string.Empty;
        }
        public static string GetText(this IWebDriver webDriver, IWebElement targetElement)
        {
            //return targetElement.Text;

            StringBuilder sbScript = new StringBuilder();
            sbScript.AppendLine($"return arguments[0].textContent;");

            IJavaScriptExecutor js = webDriver as IJavaScriptExecutor;
            return (js.ExecuteScript(sbScript.ToString(), targetElement) as string) ?? string.Empty;
        }
        public static Func<string> GetFunc(DisplayObject obj)
        {
            return new Func<string>(() =>
            {
                WebDriverWait wait = new WebDriverWait(ChromeHandle.ChromeDriver, new TimeSpan(0, 0, obj.iTimeout));
                try
                {
                    wait.Until(wd =>
                    {
                        IWebElement element = null;
                        try
                        {
                            element = wd.FindElement(By.XPath(obj.sTarget));
                        }
                        catch (Exception ex)
                        {
                            Log.Error(MethodBase.GetCurrentMethod(), ex);
                            return false;
                        }

                        try
                        {
                            DOMRect rect = element.GetElementRect();
                            Log.Debug(MethodBase.GetCurrentMethod(), element.TagName, rect);

                            return wd.GetDisplay(element).ToEqualEx(obj.sValue);
                        }
                        catch (Exception ex)
                        {
                            Log.Error(MethodBase.GetCurrentMethod(), ex);
                            return false;
                        }
                    });
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    Log.Error(MethodBase.GetCurrentMethod(), ex);
                    return ex.Message;
                }
            });
        }
    }

    public class DisplayObject
    {
        public string sTarget { get; set; } = string.Empty;
        public string sValue { get; set; } = string.Empty;
        public int iTimeout { get; set; } = Define.Instance.DelayValue;
    }
}
